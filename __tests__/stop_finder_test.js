let request = require('supertest');
let _ = require('lodash');

describe('GET /Stop-finder', function () {
    it('When user searches for "Wynyard Station"', function (done) {
        let strStationName = 'wynyard';

        //create request object
        request('https://www.transportnsw.info')
            // pass on full url for api
            .get(`/web/XML_STOPFINDER_REQUEST?TfNSWSF=true&language=en&name_sf=${strStationName}&outputFormat=rapidJSON&type_sf=any&version=10.2.2.48`)
            //expect valid http response
            .expect(200)
            //match expected result
            .then((response)=> {
                let newObj = _.filter(response.body.locations,['type','stop']);
                //as it only returns one record, so for brevity sake, I am using index number in assertion
                expect(newObj[0]).toHaveProperty('name','Wynyard Station, Sydney');
                expect(newObj[0].modes.length).toBeGreaterThan(1);
                done();
            });
    });
});